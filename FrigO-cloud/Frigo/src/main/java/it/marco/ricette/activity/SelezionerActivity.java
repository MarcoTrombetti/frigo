package it.marco.ricette.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import it.marco.ricette.R;

public class SelezionerActivity extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selezioner);
        final Button buttonr = (Button) findViewById(R.id.buttonr);
        buttonr.setOnClickListener(this);

        ImageButton btnHome=(ImageButton)findViewById(R.id.imageButton3);
        btnHome.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View arg0) {
                Intent openSelActivity = new Intent(SelezionerActivity.this,MainActivity.class);
                startActivity(openSelActivity);
            }
        });
    }
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch ( v.getId() ) {
            case R.id.buttonr:
                final EditText edit_selezioner = (EditText)findViewById(R.id.edit_selectr);

                Bundle bundle = new Bundle();
                bundle.putString("alim", edit_selezioner.getText().toString());
                bundle.putString("alim2", "1" .toString());
                String contenutor = edit_selezioner.getText().toString();

                if(contenutor.equals("")) {
                    Intent openSelActivity = new Intent(SelezionerActivity.this, SelezionecActivity.class);
                    openSelActivity.putExtras(bundle);
                    startActivity(openSelActivity);
//                    Toast.makeText(getApplicationContext(), "DatiUno:" + contenutor, Toast.LENGTH_LONG).show()  ;
                } else {
                    int length = contenutor.length();
                    if(length >2)  {
                    Intent openSelActivity = new Intent(SelezionerActivity.this, SelRActivity.class);
                    openSelActivity.putExtras(bundle);
                    startActivity(openSelActivity);
                } else {
                Toast.makeText(getApplicationContext(), "Selezione minimo 3 caratteri", Toast.LENGTH_LONG).show()  ;
            }
                }
                break;
        }
    }
}
