package it.marco.ricette.activity;

import android.os.Bundle;
import it.marco.ricette.R;
import it.marco.ricette.fragment.IngredienteDetailFragment;
import it.marco.ricette.model.Ingrediente;

public class IngredienteDetailActivity extends ToolbarActivity {
    public static final String ARG_INGREDIENTE = "ingrediente";

    private IngredienteDetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            detailFragment = IngredienteDetailFragment.newInstance((Ingrediente) getIntent().getParcelableExtra(ARG_INGREDIENTE));
            getFragmentManager().beginTransaction()
                    .add(R.id.ingrediente_detail_container, detailFragment)
                    .commit();
        } else {
            detailFragment = (IngredienteDetailFragment) getFragmentManager().findFragmentById(R.id.ingrediente_detail_container);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ingrediente_detail;
    }
}
