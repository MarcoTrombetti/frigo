package it.marco.ricette.activity;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

import it.marco.ricette.R;


//public class WebHelpActivity extends AppCompatActivity {
    public class HelpPrivacyActivity extends Activity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_help_privacy);
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            boolean connessioneDisponibile=false;
            if(cm!=null && cm.getActiveNetworkInfo()!=null){
                //controllo disponibilità di rete
                connessioneDisponibile= cm.getActiveNetworkInfo().isConnectedOrConnecting();
            }
            if(connessioneDisponibile){
                //carichiamo la pagina web
                WebView myWebView = (WebView) findViewById(R.id.webview);
                myWebView.loadUrl("http://www.frigole.cloud/home/helpl/privacy.html");
            }
            else{
                Toast t=new Toast(HelpPrivacyActivity.this);
                t.makeText(this, "Connessione non disponibile", Toast.LENGTH_SHORT).show();
            }
        }
    }
