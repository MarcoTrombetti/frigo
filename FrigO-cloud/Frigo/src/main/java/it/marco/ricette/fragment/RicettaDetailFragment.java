package it.marco.ricette.fragment;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import it.marco.ricette.R;
import it.marco.ricette.activity.LoginVotoActivity;
import it.marco.ricette.activity.ToolbarActivity;
import it.marco.ricette.model.Ricetta;
import it.marco.ricette.util.AnimUtils;
import it.marco.ricette.util.HtmlUtils;
import it.marco.ricette.view.BlurTransformation;
import it.marco.ricette.view.ObservableScrollView;
import it.marco.ricette.view.ScrollViewListener;

public class RicettaDetailFragment extends Fragment implements ScrollViewListener {

    private static final String ARG_RICETTA = "ricetta";
    private static final String STATE_RICETTA = "ricetta";

    private static final long ANIM_IMAGE_ENTER_DURATION = 500;
    private static final long ANIM_TEXT_ENTER_DURATION = 500;
    private static final long ANIM_IMAGE_ENTER_STARTDELAY = 300;
    private static final long ANIM_COLORBOX_ENTER_DURATION = 200;

    private static final TimeInterpolator decelerator = new DecelerateInterpolator();

    private Toolbar toolbar;
    private ImageView imageViewSp;
    private ImageView imageView;
    private ImageView blurredImageView;
    private TextView historyView;
    private TextView personeView;
    private ObservableScrollView scrollView;
    private TextView ingredientsView;
    private TextView instructionsView;
    private Button wikButton;
    private Button votoButton;
    private RatingBar ratingBarCView;
    private RatingBar ratingBarRView;
    private TextView votiCView;
    private TextView votiRView;
    private TextView stelleCView;
    private TextView stelleRView;
    private TextView totStars;
    private View colorBox;
    private View colorView1;
    private View colorView2;
    private View colorView3;
    private View colorView4;

    private int imageViewHeight;

    private Transformation transformation;

    private Ricetta ricetta;

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            imageView.setImageBitmap(bitmap);
            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    colorView1.setBackgroundColor(palette.getVibrantColor(0));
                    colorView2.setBackgroundColor(palette.getLightVibrantColor(0));
                    colorView3.setBackgroundColor(palette.getDarkVibrantColor(0));
                    colorView4.setBackgroundColor(palette.getMutedColor(0));
                }
            });
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            // no-op
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            // no-op
        }
    };

    public static RicettaDetailFragment newInstance(Ricetta ricetta) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_RICETTA, ricetta);
        RicettaDetailFragment fragment = new RicettaDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        toolbar = ((ToolbarActivity) getActivity()).getToolbar();
        toolbar.getBackground().setAlpha(0);

        View root = inflater.inflate(R.layout.fragment_ricetta_detail, container, false);

        imageView = (ImageView) root.findViewById(R.id.image);
        blurredImageView = (ImageView) root.findViewById(R.id.image_blurred);
        historyView = (TextView) root.findViewById(R.id.history);
        String string1 = historyView.getText().toString();

        votiCView = (TextView) root.findViewById(R.id.votiChef);                   //29102016
        votiRView = (TextView) root.findViewById(R.id.votiRicetta);                //29102016
        stelleCView = (TextView) root.findViewById(R.id.stelleChef);               //29102016
//        ratingBarCView = (RatingBar) root.findViewById(R.id.ratingBarC);           //29102016

//        ratingBarCView.setNumStars(10);
//        ratingBarCView.setRating((R.id.stelleRicetta)/300000000);

        personeView = (TextView) root.findViewById(R.id.persone);
        scrollView = (ObservableScrollView) root.findViewById(R.id.scroll);
        ingredientsView = (TextView) root.findViewById(R.id.ingredients);
        instructionsView = (TextView) root.findViewById(R.id.instructions);
        wikButton = (Button) root.findViewById(R.id.wik);
        votoButton = (Button) root.findViewById(R.id.votoButton);
        stelleRView = (TextView) root.findViewById(R.id.stelleRicetta);
//        ratingBarRView = (RatingBar) root.findViewById(R.id.ratingBarR);

//        ratingBarRView.setNumStars(10);
//        ratingBarRView.setRating((R.id.stelleRicetta)-2131558567);

        imageViewSp = (ImageView) root.findViewById(R.id.imagesr);

//        Picasso.with(context).load(R.id.imagesr).into(imageViewSp);
//        Picasso.with(this)
//                .load("https://cms-assets.tutsplus.com/uploads/users/21/posts/19431/featured_image/CodeFeature.jpg")
//                .into(imageView);
//        stelleRView = (TextView) root.findViewById(R.id.stelleRicetta);
//        totStars = (TextView) root.findViewById(R.id.stelleRicetta);

        colorBox = root.findViewById(R.id.colorbox);
        colorView1 = root.findViewById(R.id.color1);
        colorView2 = root.findViewById(R.id.color2);
        colorView3 = root.findViewById(R.id.color3);
        colorView4 = root.findViewById(R.id.color4);

        wikButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToWiksponsor();
            }
        });

        votoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToVoto();
            }
        });

        setHasOptionsMenu(true);

        ricetta = getArguments().getParcelable(ARG_RICETTA);

        getActivity().setTitle(ricetta.name);
        Picasso.with(getActivity()).load(ricetta.imageUrl).into(target);

        transformation = new BlurTransformation(getActivity(), getResources().getInteger(R.integer.blur_radius));
        Picasso.with(getActivity()).load(ricetta.imageUrl).transform(transformation).into(blurredImageView);

        imageViewHeight = (int) getResources().getDimension(R.dimen.ricetta_detail_recipe_margin);
        scrollView.setScrollViewListener(this);

        if (savedInstanceState != null) {
            colorBox.setAlpha(1);
            Ricetta ricetta = savedInstanceState.getParcelable(STATE_RICETTA);
            if (ricetta != null) {
                refreshUI(ricetta);
            }
        } else {
            imageView.setVisibility(View.INVISIBLE);
            ViewTreeObserver observer = imageView.getViewTreeObserver();
            if (observer != null) {
                observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        runEnterAnimation();
                        return true;
                    }
                });
            }
        }

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.ricetta_detail, menu);
    }

    private void goToWiksponsor() {
//        if (ricetta == null) {
            return;
//        }
//        Intent i = new Intent(getActivity(), MainActivity.class);
//        startActivity(i);
    }

    private void goToVoto() {
//        historyView = TextView.findViewById(R.id.history);
        String string1 = historyView.getText().toString();

//        Toast.makeText(RicettaDetailFragment.this,string1,Toast.LENGTH_LONG).show();
        if (ricetta == null) {
            return;
        }
        if (string1.equals("Anonimo")) {
            return;
        }

        Intent i = new Intent(getActivity(), LoginVotoActivity.class);
        i.putExtra("ricettaNome", (ricetta.name));
        startActivity(i);
    }

    @TargetApi(21)
    private void runEnterAnimation() {
        if (Build.VERSION.SDK_INT >= 21) {
            int cx = imageView.getWidth() / 2;
            int cy = imageView.getHeight() / 2;

            // OMG some Pythagorean theorem
            int finalRadius =
                    (int) Math.sqrt(Math.pow(imageView.getWidth(), 2) + Math.pow(imageView.getHeight(), 2)) / 2;

            Animator animator = ViewAnimationUtils.createCircularReveal(imageView, cx, cy, 0, finalRadius);
            animator.setDuration(ANIM_IMAGE_ENTER_DURATION);
            animator.setStartDelay(ANIM_IMAGE_ENTER_STARTDELAY);
            animator.setInterpolator(decelerator);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    imageView.setVisibility(View.VISIBLE);
                    refreshUI(ricetta);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    colorBox.animate()
                            .alpha(1)
                            .setDuration(ANIM_COLORBOX_ENTER_DURATION)
                            .setInterpolator(decelerator);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.start();
        } else {
            imageView.setVisibility(View.VISIBLE);
            Runnable refreshRunnable = new Runnable() {
                @Override
                public void run() {
                    refreshUI(ricetta);
                }
            };
            imageView.setTranslationY(-imageView.getHeight());

            ViewPropertyAnimator animator = imageView.animate().setDuration(ANIM_IMAGE_ENTER_DURATION).
                    setStartDelay(ANIM_IMAGE_ENTER_STARTDELAY).
                    translationY(0).
                    setInterpolator(decelerator);

            Runnable animateColorBoxRunnable = new Runnable() {
                @Override
                public void run() {
                    colorBox.animate()
                            .alpha(1)
                            .setDuration(ANIM_COLORBOX_ENTER_DURATION)
                            .setInterpolator(decelerator);
                }
            };

            AnimUtils.scheduleStartAction(animator, refreshRunnable, ANIM_IMAGE_ENTER_STARTDELAY);
            AnimUtils.scheduleEndAction(animator, animateColorBoxRunnable, ANIM_IMAGE_ENTER_STARTDELAY, ANIM_IMAGE_ENTER_DURATION);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (ricetta != null) {
            outState.putParcelable(STATE_RICETTA, ricetta);
        }
    }

    @Override
    public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {
        float alpha = 2 * (float) y / (float) imageViewHeight;
        if (alpha > 1) {
            alpha = 1;
        } else if (alpha < 0) {
            alpha = 0;
        }
        blurredImageView.setAlpha(alpha);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) imageView.getLayoutParams();
        params.setMargins(params.leftMargin, -y / 2, params.rightMargin, params.bottomMargin);
        imageView.setLayoutParams(params);

        params = (FrameLayout.LayoutParams) blurredImageView.getLayoutParams();
        params.setMargins(params.leftMargin, -y / 2, params.rightMargin, params.bottomMargin);
        blurredImageView.setLayoutParams(params);

        toolbar.getBackground().setAlpha((int) (alpha * 255));
    }

    public void refreshUI(Ricetta ricetta) {
        this.ricetta = ricetta;

        if (getActivity() == null) {
            return;
        }

        historyView.setText(ricetta.history);
        personeView.setText(ricetta.persone);
        votiCView.setText(ricetta.votiChef);                  //29102016
        votiRView.setText(ricetta.votiRicetta);               //29102016
        stelleCView.setText(ricetta.stelleChef);              //29102016  stampa
        stelleRView.setText(ricetta.stelleRicetta);           //29102016  stampa

//        totStars.setText(ricetta.stelleChef);

        ingredientsView.setText(Html.fromHtml(HtmlUtils.getIngredientsHtml(this.ricetta)));

        instructionsView.setText(ricetta.instructions);
//        wikButton.setText(String.format("Buon appetito!!!") + ricetta.name);
        wikButton.setText(String.format (ricetta.name));
//        wikButton.setText(String.format(getString(R.string.ricetta_detail_wikipedia), ricetta.name));

        ViewTreeObserver observer = scrollView.getViewTreeObserver();
        if (observer != null) {
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    scrollView.getViewTreeObserver().removeOnPreDrawListener(this);
                    scrollView.setAlpha(0);
                    scrollView.animate().setDuration(ANIM_TEXT_ENTER_DURATION).
                            alpha(1).
                            setInterpolator(decelerator);
                    // Fake a onScrollChangedCall to apply changes to blurredImageView and imageView.
                    fakeOnScrollChanged();
                    return true;
                }
            });
        }
        scrollView.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
/***
            case R.id.menu_item_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, ricetta.name);
                sendIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(HtmlUtils.getIngredientsHtml(ricetta)));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                return true;
*/
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    private void fakeOnScrollChanged() {
        onScrollChanged(scrollView, scrollView.getScrollX(),
                scrollView.getScrollY(), scrollView.getScrollX(), scrollView.getScrollY());
    }
}
