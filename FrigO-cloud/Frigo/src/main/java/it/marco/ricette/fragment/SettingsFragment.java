package it.marco.ricette.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.View;

import it.marco.ricette.R;
import it.marco.ricette.data.RicetteProvider;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (!isAdded()) {
            return;
        }
        if (key.equals(getString(R.string.pref_apiendpoint_key))) {

            String server = sharedPreferences.getString(key, getString(R.string.pref_apiendpoint_default));
            RicetteProvider.updateServer(server);

            View view = getView();
            if (view != null) {
                Snackbar.make(view, R.string.server_updated, Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
