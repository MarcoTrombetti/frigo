package it.marco.ricette.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ParseJSON {
    public static String[] names;
    public static String[] ids;
    public static String[] cat;
//    public static String[] totrice;

    public static final String JSON_ARRAY = "resultc";
    public static final String KEY_NAME = "descrizione";
    public static final String KEY_ID = "num_ricette";
    public static final String KEY_CAT = "categorie_id";
//    public static final String KEY_TOTRICE = "";

    private JSONArray users = null;

    private String json;

    public ParseJSON(String json){
        this.json = json;
    }

    public void parseJSON(){
        JSONObject jsonObject=null;
        int tot=0;
        try {
            jsonObject = new JSONObject(json);
            users = jsonObject.getJSONArray(JSON_ARRAY);

            ids = new String[users.length()];
            names = new String[users.length()];
            cat = new String[users.length()];
//            totrice = new String[users.length()];

            for(int i=0;i<users.length();i++){
                JSONObject jo = users.getJSONObject(i);
                ids[i] = jo.getString(KEY_ID);
                names[i] = jo.getString(KEY_NAME);
                cat[i] = jo.getString(KEY_CAT);
//                totrice[i] = jo.getString(KEY_TOTRICE);
                int intero = Integer.parseInt(ids[i]);
                tot+=intero;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
