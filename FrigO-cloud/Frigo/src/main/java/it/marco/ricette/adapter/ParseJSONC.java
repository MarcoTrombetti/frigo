package it.marco.ricette.adapter;

/**
 * Created by Marco on 06/12/2016.
 */

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;


public class ParseJSONC {
    public static String[] names;
    public static String[] ids;
//    public static String[] emails;

    public static final String A_JSON_ARRAY = "resulta";
    public static final String A_KEY_NAME = "nick";
    public static final String A_KEY_ID = "chef_id";
//    public static final String KEY_EMAIL = "riga";

    private JSONArray users = null;

    private String json;

    public ParseJSONC(String json){
        this.json = json;
    }

    public void parseJSONC(){
        JSONObject jsonObject=null;
        try {
            jsonObject = new JSONObject(json);
            users = jsonObject.getJSONArray(A_JSON_ARRAY);

            names = new String[users.length()];
            ids = new String[users.length()];
//            emails = new String[users.length()];

            for(int i=0;i<users.length();i++){
                JSONObject jo = users.getJSONObject(i);
                names[i] = jo.getString(A_KEY_NAME);
                ids[i] = jo.getString(A_KEY_ID);
//                emails[i] = jo.getString(KEY_EMAIL);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
