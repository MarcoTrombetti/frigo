package it.marco.ricette.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import it.marco.ricette.R;


public class ChefListAdapter extends ArrayAdapter<String> {
    private String[] names;
    private String[] ids;
    //    private String[] emails;
    private Activity context;

    //        public CategoriaListAdapter(Activity context, String[] ids, String[] names, String[] emails) {
    public ChefListAdapter(Activity context, String[] names, String[] ids) {
        super(context, R.layout.list_view_layout_c, ids);
        this.context = context;
        this.names = names;
        this.ids = ids;
//        this.emails = emails;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_view_layout_c, null, true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
//        TextView textViewEmail = (TextView) listViewItem.findViewById(R.id.textViewEmail);

        textViewName.setText(names[position]);
        textViewId.setText(ids[position]);
//        textViewEmail.setText(emails[position]);

        return listViewItem;
    }
}
