package it.marco.ricette.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import it.marco.ricette.R;

public class LoginVotoActivity extends AppCompatActivity {

    private static final String TAG = "LoginVotoActivity";
    private static final String URL_FOR_LOGIN = "http://www.frigole.cloud/home/apir5/loginvoto.php";
    ProgressDialog progressDialog;
//    private EditText loginInputEmail, loginInputPassword, loginInputVoto;
    private EditText loginInputEmail, loginInputPassword, loginInputVoto;
    private TextView loginInputRicetta;
    private Button btnlogin;
//    private Button btnLinkSignup;
//    private Button buttonRitorna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_voto);

        Bundle bundle = getIntent().getExtras();
        String ricetta_nome = bundle.getString("ricettaNome");

        //Hidden Tastiera Default
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        loginInputEmail = (EditText) findViewById(R.id.login_input_email);
        loginInputPassword = (EditText) findViewById(R.id.login_input_password);
        loginInputVoto = (EditText) findViewById(R.id.login_input_voto);
        loginInputVoto.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "10")});
        loginInputRicetta = (TextView) findViewById(R.id.login_input_ricetta);

        btnlogin = (Button) findViewById(R.id.votoButton);
        loginInputRicetta.setText(ricetta_nome);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser(loginInputEmail.getText().toString(),
                        loginInputPassword.getText().toString());
//                        loginInputVoto.getText().toString());
//                        loginInputRicetta.getText().toString());
            }
        });

/***
        btnLinkSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        buttonRitorna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                startActivity(i);
            }
        });
*/
    }   //on create

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Sei sicuro di voler uscire? I dati non inviati andranno persi.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        LoginVotoActivity.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


//    private void loginUser( final String email, final String password, final String voto, final String ricetta) {
    private void loginUser( final String email, final String password) {
        // Tag used to cancel the request
        String cancel_req_tag = "login";
        progressDialog.setMessage("Logging you in...");
        showDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                URL_FOR_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    if (!error) {
                        String user = jObj.getJSONObject("user").getString("name");
                        String mail = email;
//                        String password = loginInputPassword.getText().toString();
//                        String ricetta = loginInputVoto.getText().toString();
                        String voto = loginInputVoto.getText().toString();
                        // Launch User activity
                        Intent intent = new Intent(
                                LoginVotoActivity.this,
                                UserVotoActivity.class);
                        intent.putExtra("username", user);
                        intent.putExtra("usermail", mail);
                        intent.putExtra("userricetta", loginInputRicetta.getText().toString());
                        intent.putExtra("uservoto", voto);
                        startActivity(intent);
                        finish();

                    } else {

                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
//                params.put("ricetta", ricetta);
//                params.put("voto", voto);
                return params;
            }

        };
        // Adding request to request queue
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq,cancel_req_tag);
    }

    private void showDialog() {
        if (!progressDialog.isShowing())
            progressDialog.show();
    }
    private void hideDialog() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public class InputFilterMinMax implements InputFilter {

        private int min, max;

        public InputFilterMinMax(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public InputFilterMinMax(String min, String max) {
            this.min = Integer.parseInt(min);
            this.max = Integer.parseInt(max);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(min, max, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}


