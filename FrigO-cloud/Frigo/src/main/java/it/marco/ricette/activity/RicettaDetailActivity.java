package it.marco.ricette.activity;

import android.os.Bundle;
import it.marco.ricette.R;
import it.marco.ricette.fragment.RicettaDetailFragment;
import it.marco.ricette.model.Ricetta;

public class RicettaDetailActivity extends ToolbarActivity {

    public static final String ARG_RICETTA = "ricetta";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            RicettaDetailFragment fragment = RicettaDetailFragment.newInstance((Ricetta) getIntent().getParcelableExtra(ARG_RICETTA));
            getFragmentManager().beginTransaction()
                    .add(R.id.ricetta_detail_container, fragment)
                    .commit();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_ricetta_detail;
    }
}
