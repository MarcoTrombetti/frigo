package it.marco.ricette.util;

import it.marco.ricette.model.Ricetta;

public class HtmlUtils {
    public static String getIngredientsHtml(Ricetta ricetta) {
        StringBuilder builder = new StringBuilder();
        int i = 0;
        for (String ingredient : ricetta.ingredients) {
            builder.append("&#8226; ");
            builder.append(ingredient);
            if (++i < ricetta.ingredients.size()) {
                builder.append("<br>");
            }
        }
        return builder.toString();
    }
}
