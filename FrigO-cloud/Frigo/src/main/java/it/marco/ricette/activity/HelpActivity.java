package it.marco.ricette.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import it.marco.ricette.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Button btnHome=(Button)findViewById(R.id.buttonHelp);
        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openHelpGuidaActivity = new Intent(HelpActivity.this,HelpGuidaActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openHelpGuidaActivity);
            }
        });

        Button btn2Home=(Button)findViewById(R.id.buttonSign);
        btn2Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg2) {
                // definisco l'intenzione
                Intent openLoginActivity = new Intent(HelpActivity.this,LoginActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openLoginActivity);
            }
        });

        Button btn3Home=(Button)findViewById(R.id.btRegistra);
        btn3Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openRegisterActivity = new Intent(HelpActivity.this,RegisterActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openRegisterActivity);
            }
        });

        Button btn5Home=(Button)findViewById(R.id.btPrivacy);
        btn5Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openHelpPrivacyActivity = new Intent(HelpActivity.this,HelpPrivacyActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openHelpPrivacyActivity);
            }
        });

/******
        Button btn4Home=(Button)findViewById(R.id.btEsci);
        btn4Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openMainActivity = new Intent(HelpActivity.this,MainActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openMainActivity);
            }
        });
*/

        ImageButton btn6Home= (ImageButton) findViewById(R.id.imageButton8);
        btn6Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openMainActivity = new Intent(HelpActivity.this,MainActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openMainActivity);
            }
        });

/******
        Button btn2Home=(Button)findViewById(R.id.buttonSign);
        btn2Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg2) {
                // definisco l'intenzione
                Intent openWebSignActivity = new Intent(HelpActivity.this,WebLoginActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openWebSignActivity);
            }
        });

*/

    }
    }
