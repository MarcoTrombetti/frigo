package it.marco.ricette.view;

public interface ViewPagerScrollListener {
    public void onScroll(int position, float positionOffset, int positionOffsetPixels);
}
