package it.marco.ricette.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import it.marco.ricette.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/***
        Spinner spinner = (Spinner)findViewById(R.id.spinnerM);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                new String[]{"Ingrediente","Ricetta","Chef"}
        );
        spinner.setAdapter(adapter);
*/

        //Identifico ilbottone tramite il proprio id
        ImageButton btnHome=(ImageButton)findViewById(R.id.imageButton);
        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg1) {
                Intent openSelezioneActivity = new Intent(MainActivity.this,SelezioneActivity.class);
                startActivity(openSelezioneActivity);
            }
        });

        //Identifico ilbottone tramite il proprio id
        ImageButton btn9Home=(ImageButton)findViewById(R.id.imageButton9);
        btn9Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                Intent openHelpActivity = new Intent(MainActivity.this,HelpActivity.class);
                startActivity(openHelpActivity);
            }
        });
    }  //onCreate


}
