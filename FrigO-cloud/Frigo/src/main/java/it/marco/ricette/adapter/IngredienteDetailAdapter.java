package it.marco.ricette.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import it.marco.ricette.view.Holder;
import it.marco.ricette.R;
import it.marco.ricette.model.Ricetta;

import java.util.ArrayList;
import java.util.List;

public class IngredienteDetailAdapter extends BaseAdapter {
    private List<Ricetta> ricette = new ArrayList<Ricetta>();
    private Context context;

    public IngredienteDetailAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return ricette.size();
    }

    @Override
    public Ricetta getItem(int i) {
        return ricette.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View root, ViewGroup parent) {

        if (root == null) {
            root = LayoutInflater.from(context).inflate(R.layout.item_ingrediente_detail, parent, false);
        }

        final TextView nameView = Holder.get(root, R.id.name);
        final ImageView imageView = Holder.get(root, R.id.image);

        final Ricetta ricetta = getItem(i);

        nameView.setText(ricetta.name);
        Picasso.with(context).load(ricetta.imageUrl).into(imageView);

        return root;
    }

    public void update(List<Ricetta> ricette) {
        this.ricette = ricette;
        notifyDataSetChanged();
    }

    public ArrayList<Ricetta> getRicette() {
        return (ArrayList<Ricetta>) ricette;
    }
}
