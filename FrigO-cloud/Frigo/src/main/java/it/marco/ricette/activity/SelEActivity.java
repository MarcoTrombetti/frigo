package it.marco.ricette.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import it.marco.ricette.R;
import it.marco.ricette.RicetteApplication;
import it.marco.ricette.adapter.AlimentoListAdapter;
import it.marco.ricette.adapter.ParseJSONA;

public class SelEActivity extends AppCompatActivity {

//    public static final String JSON_URL = "http://simplifiedcoding.16mb.com/UserRegistration/json.php";
    public static String jsonUrl1 = "";
    public static String jsonUrl = "http://www.frigole.cloud/home/api7/S7alimento.php?desc=";

//    private Button buttonGet;

    private ListView listViewA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sele);

        ImageButton btnHome=(ImageButton)findViewById(R.id.imageButton6);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent openSelActivity = new Intent(SelEActivity.this, MainActivity.class);
                startActivity(openSelActivity);
            }
        });

        String desAlim = getIntent().getExtras().getString("alim");
        String alim = getIntent().getExtras().getString("alim").replace(" ","%20");
//        String alim = getIntent().getExtras().getString("alim");
//        String s = ((RicetteApplication) this.getApplication()).getSomeVariable();
        ((RicetteApplication) this.getApplication()).setSomeVariable(desAlim);

        jsonUrl1 = jsonUrl + alim;
//        jsonUrl1 = jsonUrl + "nodino di vitello";
//                        Toast.makeText(getApplicationContext(), jsonUrl1, Toast.LENGTH_LONG).show()  ;
        sendRequest();
        TextView testo = (TextView) findViewById(R.id.textViewA3);
//        testo.setText(alim);
        testo.setText("Ingrediente: " + desAlim);
//        buttonGet = (Button) findViewById(R.id.buttonGet);
//        buttonGet.setOnClickListener(this);
        listViewA = (ListView) findViewById(R.id.listViewA);

    }

    private void sendRequest(){

        StringRequest stringRequest = new StringRequest(jsonUrl1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        showJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SelEActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void showJSON(String json){
        ParseJSONA pj = new ParseJSONA(json);
        pj.parseJSONA();
        AlimentoListAdapter cl = new AlimentoListAdapter(this, ParseJSONA.names,ParseJSONA.ids);
        listViewA.setAdapter(cl);

 int rows = listViewA.getCount();
        if(rows==0) {
            Toast.makeText(SelEActivity.this,"Nessuna o troppe corrispondenze.",Toast.LENGTH_LONG).show();
            Toast.makeText(SelEActivity.this,"Cambia ricerca!",Toast.LENGTH_LONG).show();
            finish();
        }
        if(rows>=100) {
            Toast.makeText(SelEActivity.this,"Troppe corrispondenze.",Toast.LENGTH_LONG).show();
            Toast.makeText(SelEActivity.this,"Cambia ricerca!",Toast.LENGTH_LONG).show();
            finish();
        }

        listViewA.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listViewA, View itemView, int position,
                                    long itemId) {
                Log.d("SelEActivity.onCreate", "Hai selezionato " + listViewA.getItemAtPosition(position).toString());
                Log.d("SelEActivity.onCreate", "con id = " + itemId + " e posizione = " + position);

                Bundle bundle = new Bundle();
                bundle.putString("codSinonimo", listViewA.getItemAtPosition(position).toString());
                Intent codSinonimo = new Intent(SelEActivity.this, SelFActivity.class);
                codSinonimo.putExtras(bundle);
                startActivity(codSinonimo);

            }
        });
    }

//    @Override
//    public void onClick(View v) {
//        sendRequest();
//    }
}
