package it.marco.ricette.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.marco.ricette.R;
import it.marco.ricette.model.Ricetta;

public class RicetteListAdapter extends RecyclerView.Adapter<RicetteListAdapter.ViewHolder> implements Filterable {
    private List<Ricetta> ricette = Collections.emptyList();
    private List<Ricetta> savedRicette = Collections.emptyList();
    private OnItemClickListener onItemClickListener;
    private Filter filter = new Filter() {

        private List<Ricetta> filteredRicette = new ArrayList<>();

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            filteredRicette.clear();
            FilterResults results = new FilterResults();

            Log.d("adapter", "performing filtering with " + charSequence);

            final String query = charSequence.toString().toLowerCase();

            final List<Ricetta> ingredientMatchRicette = new ArrayList<Ricetta>();

            for (Ricetta ricetta : savedRicette) {
                if (ricetta.name.toLowerCase().contains(query)) {
                    filteredRicette.add(ricetta);
                } else {
                    // ricetta name does not match, we check the ingredients
                    for (String ingredient : ricetta.ingredients) {
                        if (ingredient.toLowerCase().contains(query)) {
                            ingredientMatchRicette.add(ricetta);
                            break;
                        }
                    }
                }
            }

            filteredRicette.addAll(ingredientMatchRicette);

            results.count = filteredRicette.size();
            results.values = filteredRicette;

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            update((List<Ricetta>) filterResults.values, true);
        }
    };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ricette_list, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final Ricetta ricetta = ricette.get(position);
        viewHolder.nameView.setText(ricetta.name);
        Picasso.with(viewHolder.imageView.getContext()).load(ricetta.imageUrl).into(viewHolder.imageView);
        if (onItemClickListener != null) {
            viewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onClick(v, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return ricette.size();
    }

    public void update(List<Ricetta> ricette, boolean dueToFilterOperation) {
        this.ricette = ricette;
        notifyDataSetChanged();
        if (!dueToFilterOperation) {
            savedRicette = new ArrayList<>(ricette);
        }
    }

    public void update(List<Ricetta> ricette) {
        update(ricette, false);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public ArrayList<Ricetta> getRicette() {
        return (ArrayList<Ricetta>) ricette;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final TextView nameView;
        private final View rootView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.image);
            nameView = (TextView) itemView.findViewById(R.id.name);
        }
    }

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }
}
