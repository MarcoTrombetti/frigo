package it.marco.ricette.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import it.marco.ricette.R;
import it.marco.ricette.RicetteApplication;
import it.marco.ricette.adapter.CategoriaListAdapter;
import it.marco.ricette.adapter.ParseJSON;

public class SelFActivity extends AppCompatActivity {

//    public static final String JSON_URL = "http://simplifiedcoding.16mb.com/UserRegistration/json.php";
    public static String jsonUrl2 = "";
    public static String jsonUrl1 = "";
    public static String jsonUrl = "http://www.t4srl.it/home/api7/S7categorie.php?desc=";

//    private Button buttonGet;

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self);

        ImageButton btnHome=(ImageButton)findViewById(R.id.imageButton4);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent openSelActivity = new Intent(SelFActivity.this, MainActivity.class);
                startActivity(openSelActivity);
            }
        });

//        String s = ((RicetteApplication) this.getApplication()).getSomeVariable();
        String codSinonimo = getIntent().getExtras().getString("codSinonimo");
//        Toast.makeText(getApplicationContext(), "Dati:" + codSinonimo, Toast.LENGTH_LONG).show()  ;
        String ra = ((RicetteApplication) this.getApplication()).getSomeVariable();
//        ((RicetteApplication) this.getApplication()).setSomeVariable("/phpricette.php");

        jsonUrl1 = jsonUrl + codSinonimo;
        sendRequest();
        TextView testo = (TextView) findViewById(R.id.textView3);
        testo.setText("Ingrediente: " + ra);
//        buttonGet = (Button) findViewById(R.id.buttonGet);
//        buttonGet.setOnClickListener(this);
        listView = (ListView) findViewById(R.id.listView);

    }

/***
    public boolean saveFile(Context context, String mytext){
        Log.i("TESTE", "SAVE");
        try {
            FileOutputStream fos = context.openFileOutput("file_name"+".txt", Context.MODE_PRIVATE);
            Writer out = new OutputStreamWriter(fos);
            out.write(mytext);
            out.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
*/
    private void sendRequest(){

        StringRequest stringRequest = new StringRequest(jsonUrl1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Toast.makeText(getApplicationContext(), "Dati:Tutto OK", Toast.LENGTH_LONG).show()  ;
                        showJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SelFActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void sendRequest2(){

        StringRequest stringRequest2 = new StringRequest(jsonUrl2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response2) {
//                        Toast.makeText(getApplicationContext(), jsonUrl2, Toast.LENGTH_LONG).show()  ;
//                        showJSON(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SelFActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        RequestQueue requestQueue2 = Volley.newRequestQueue(this);
        requestQueue2.add(stringRequest2);
    }

    private void showJSON(String json){
        ParseJSON pj = new ParseJSON(json);
        pj.parseJSON();
        CategoriaListAdapter cl = new CategoriaListAdapter(this, ParseJSON.ids,ParseJSON.names,ParseJSON.cat);
        listView.setAdapter(cl);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View itemView, int position,
                                    long itemId) {

                String codCategoria = (String) listView.getItemAtPosition(position);
                String codSinonimo = getIntent().getExtras().getString("codSinonimo");

//                String Url3 = "http://www.t4srl.it/home/api/phpuserU.php?desc=";
//                jsonUrl2 = Url3 + codSinonimo + "&cate=" + codCategoria + "&ricing=2&user=1";
//                sendRequest2();

                SharedPreferences settings = getSharedPreferences("FRIGO", 0); // Maurizio
                SharedPreferences.Editor editor = settings.edit(); // Maurizio
                editor.putString("descValue", codSinonimo); // Maurizio
                editor.putString("cateValue", codCategoria); // Maurizio
                editor.putString("ricingValue", "2"); // Maurizio
                editor.commit(); // Maurizio

                Intent SelActivity = new Intent(SelFActivity.this, PreselActivity.class);
//                SelActivity.putExtra("Sinonimo", codSinonimo);
//                SelActivity.putExtra("Categoria", codCategoria);

                startActivity(SelActivity);

            }
        });
    }

//    @Override
//    public void onClick(View v) {
//        sendRequest();
//    }
}
