package it.marco.ricette.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Ingrediente implements Parcelable {
    public String name;
    public String imageUrl;
    public String history;
    public String wiksponsori;
    public List<String> otherNames = new ArrayList<String>();

    public Ingrediente(Parcel parcel) {
        name = parcel.readString();
        imageUrl = parcel.readString();
        history = parcel.readString();
        wiksponsori = parcel.readString();
        parcel.readStringList(otherNames);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(imageUrl);
        parcel.writeString(history);
        parcel.writeString(wiksponsori);
        parcel.writeStringList(otherNames);
    }

    public static final Parcelable.Creator<Ingrediente> CREATOR = new Parcelable.Creator<Ingrediente>() {
        public Ingrediente createFromParcel(Parcel in) {
            return new Ingrediente(in);
        }

        public Ingrediente[] newArray(int size) {
            return new Ingrediente[size];
        }
    };
}




