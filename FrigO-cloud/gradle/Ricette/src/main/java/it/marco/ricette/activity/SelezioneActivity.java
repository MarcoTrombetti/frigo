package it.marco.ricette.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import it.marco.ricette.R;

public class SelezioneActivity extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selezione);
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);

        ImageButton btnHome=(ImageButton)findViewById(R.id.imageButton2);
        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                Intent openSelActivity = new Intent(SelezioneActivity.this,MainActivity.class);
                startActivity(openSelActivity);
            }
        });
    }
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch ( v.getId() ) {
            case R.id.button:
                final EditText edit_selezione = (EditText)findViewById(R.id.edit_select);

                Bundle bundle = new Bundle();
                bundle.putString("alim", edit_selezione.getText().toString());
                String contenuto = edit_selezione.getText().toString();


                if(contenuto.equals("")) {
                    Intent openSelActivity = new Intent(SelezioneActivity.this, SelezionerActivity.class);
                    openSelActivity.putExtras(bundle);
                    startActivity(openSelActivity);
//                    Toast.makeText(getApplicationContext(), "DatiUno:" + contenuto, Toast.LENGTH_LONG).show()  ;
                } else {
                    Intent openSelActivity = new Intent(SelezioneActivity.this, SelEActivity.class);
                    openSelActivity.putExtras(bundle);
                    startActivity(openSelActivity);
//                    Toast.makeText(getApplicationContext(), "DatiDue:" + contenuto, Toast.LENGTH_LONG).show()  ;
                }
                break;
        }
    }
}
