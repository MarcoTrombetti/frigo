package it.marco.ricette.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import it.marco.ricette.R;


public class CategoriaListAdapter extends ArrayAdapter<String> {
    private String[] ids;
    private String[] names;
    private String[] cat;
//    private String[] emails;
    private Activity context;

//        public CategoriaListAdapter(Activity context, String[] ids, String[] names, String[] emails) {
        public CategoriaListAdapter(Activity context, String[] ids, String[] names, String[] cat) {
        super(context, R.layout.list_view_layout, cat);
        this.context = context;
        this.ids = ids;
        this.names = names;
        this.cat = cat;
//        this.totrice = totrice;
//        this.emails = emails;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_view_layout, null, true);
        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewCat = (TextView) listViewItem.findViewById(R.id.textViewCat);
//        TextView textViewEmail = (TextView) listViewItem.findViewById(R.id.textViewEmail);

        textViewId.setText(ids[position]);
        textViewName.setText(names[position]);
        textViewCat.setText(cat[position]);
//        textViewTotrice.setText(totrice[position]);
//        textViewEmail.setText(emails[position]);

        return listViewItem;
    }
}
