package it.marco.ricette.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import it.marco.ricette.R;
import it.marco.ricette.model.Ingrediente;

import java.util.ArrayList;
import java.util.List;

public class IngredienteListAdapter extends RecyclerView.Adapter<IngredienteListAdapter.ViewHolder> {
    private List<Ingrediente> ingredienti = new ArrayList<Ingrediente>();
    private OnItemClickListener onItemClickListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ingredienti_list, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Ingrediente ingrediente = ingredienti.get(position);

        holder.nameView.setText(ingrediente.name);
        Picasso.with(holder.imageView.getContext()).load(ingrediente.imageUrl).into(holder.imageView);
        if (onItemClickListener != null) {
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onClick(v, position);
                }
            });
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return ingredienti.size();
    }

    public void update(List<Ingrediente> ingredienti) {
        this.ingredienti = ingredienti;
        notifyDataSetChanged();
    }

    public ArrayList<Ingrediente> getIngredienti() {
        return (ArrayList<Ingrediente>) ingredienti;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final TextView nameView;
        private final ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            nameView = (TextView) itemView.findViewById(R.id.name);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }
}
