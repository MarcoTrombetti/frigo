package it.marco.ricette.data;

import java.util.List;

import it.marco.ricette.model.Ingrediente;
import it.marco.ricette.model.Ricetta;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface RicetteService {

//    @GET("/phpricetteS.php?user=1")
//    public void getAllRicette(Callback<List<Ricetta>> callback);

    @GET("/S7ricette.php")
    public void getAllRicette2(@Query("desc") String desc, @Query("cate") String cate, @Query("ricing") String ricing, Callback<List<Ricetta>> callback);

//    @GET("/phpingredientiS.php?user=8")
//    public void getAllIngredienti(Callback<List<Ingrediente>> callback);

    @GET("/S7ingredienti.php")
    public void getAllIngredienti2(@Query("desc") String desc, @Query("cate") String cate, @Query("ricing") String ricing, Callback<List<Ingrediente>> callback);

//    @GET("/alimenti.php?valida=0")
//    public void getOneAlimento(Callback<List<Alimento>> callback);

//    String ra = ((RicetteApplication) this.getApplication()).getSomeVariable();

}
