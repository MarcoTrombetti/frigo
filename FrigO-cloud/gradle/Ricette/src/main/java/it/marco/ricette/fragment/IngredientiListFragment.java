package it.marco.ricette.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import it.marco.ricette.R;
import it.marco.ricette.activity.IngredienteDetailActivity;
import it.marco.ricette.activity.PreselActivity;
import it.marco.ricette.adapter.IngredienteListAdapter;
import it.marco.ricette.data.RicetteProvider;
import it.marco.ricette.model.Ingrediente;
import it.marco.ricette.view.RicetteOnScrollListener;
import it.marco.ricette.view.ViewPagerScrollListener;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class IngredientiListFragment extends Fragment implements Callback<List<Ingrediente>>, ViewPagerScrollListener {
    private static final String STATE_LIST = "ingrediente";
    private static final String PREF_INGREDIENTI_JSON = "ingredienti_json";

    private RecyclerView recyclerView;
    private View emptyView;
    private ProgressBar progressBar;

    private IngredienteListAdapter adapter;

    private boolean loadingError = false;

    public static IngredientiListFragment newInstance() {
        return new IngredientiListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        final View root = inflater.inflate(R.layout.fragment_ingredienti_list, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.list);
        emptyView = root.findViewById(android.R.id.empty);
        progressBar = (ProgressBar) root.findViewById(R.id.progressbar);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnScrollListener(new RicetteOnScrollListener(RicetteOnScrollListener.NAMEVIEW_POSITION_TOP));

        adapter = new IngredienteListAdapter();
        adapter.setOnItemClickListener(new IngredienteListAdapter.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                openIngredienteDetail(position);
            }
        });

        recyclerView.setAdapter(adapter);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(STATE_LIST)) {
                List<Ingrediente> savedIngredienti = savedInstanceState.getParcelableArrayList(STATE_LIST);
                updateList(savedIngredienti);
            } else {
                refresh();
            }
        } else {
            refresh();
        }

        return root;
    }

    private void updateList(List<Ingrediente> alimenti) {
        adapter.update(alimenti);
        if (adapter.getItemCount() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void openIngredienteDetail(int position) {
        Ingrediente ingrediente = adapter.getIngredienti().get(position);
        Intent intent = new Intent(getActivity(), IngredienteDetailActivity.class);
        intent.putExtra(IngredienteDetailActivity.ARG_INGREDIENTE, ingrediente);
        startActivity(intent);
    }

    @Override
    public void success(List<Ingrediente> ingredienti, Response response) {
        loadingError = false;

        if (getActivity() == null) {
            return;
        }

        progressBar.setVisibility(View.GONE);
        updateList(ingredienti);

        Gson gson = new Gson();
        String json = gson.toJson(ingredienti);
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
        editor.putString(PREF_INGREDIENTI_JSON, json);
        editor.apply();
    }

    @Override
    public void failure(RetrofitError retrofitError) {
        loadingError = true;

        if (getActivity() == null) {
            return;
        }

        progressBar.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onScroll(int position, float positionOffset, int positionOffsetPixels) {
        if (position == getResources().getInteger(R.integer.position_fragment_ingredienti)) {
            return;
        }

        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();

        int first = manager.findFirstVisibleItemPosition();
        int last = manager.findLastVisibleItemPosition();

        for (int i = 0; i <= last - first; i++) {
            View itemRoot = recyclerView.getChildAt(i);
            if (itemRoot == null) {
                continue;
            }

            TextView nameView = (TextView) itemRoot.findViewById(R.id.name);

            int textWidth = nameView.getWidth();

            nameView.setRight(Math.round(textWidth - (1 - positionOffset) * textWidth));
            nameView.setLeft(Math.round(-(1 - positionOffset) * textWidth));
        }
    }

    private void refresh() {
        progressBar.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        ((PreselActivity) getActivity()).setRefreshActionVisible(false);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (preferences.contains(PREF_INGREDIENTI_JSON)) {
            Gson gson = new Gson();
            //TODO async
            List<Ingrediente> ingredienti = gson.fromJson(preferences.getString(PREF_INGREDIENTI_JSON, "null"), new TypeToken<List<Ingrediente>>() {
            }.getType());
            updateList(ingredienti);
            progressBar.setVisibility(View.GONE);
        }
        RicetteProvider.getAllIngredienti(this,getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        if (loadingError) {
            ((PreselActivity) getActivity()).setRefreshActionVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.retry:
                refresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (adapter.getItemCount() > 0) {
            outState.putParcelableArrayList(STATE_LIST, adapter.getIngredienti());
        }
    }
}
