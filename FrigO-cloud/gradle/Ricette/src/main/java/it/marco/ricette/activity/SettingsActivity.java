package it.marco.ricette.activity;

import android.os.Bundle;
import android.view.MenuItem;
import it.marco.ricette.R;
import it.marco.ricette.fragment.SettingsFragment;

public class SettingsActivity extends ToolbarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(R.id.content, SettingsFragment.newInstance())
                .commit();

        getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }
}
