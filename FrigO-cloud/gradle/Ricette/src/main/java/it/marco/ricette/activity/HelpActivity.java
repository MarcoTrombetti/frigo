package it.marco.ricette.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import it.marco.ricette.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Button btnHome=(Button)findViewById(R.id.buttonHelp);
        btnHome.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // definisco l'intenzione
                Intent openWebHelpActivity = new Intent(HelpActivity.this,WebHelpActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openWebHelpActivity);
            }
        });
/******
        Button btn2Home=(Button)findViewById(R.id.buttonSign);
        btn2Home.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg2) {
                // definisco l'intenzione
                Intent openWebSignActivity = new Intent(HelpActivity.this,WebLoginActivity.class);
                // passo all'attivazione dell'activity Pagina.java
                startActivity(openWebSignActivity);
            }
        });

*/

    }
    }
