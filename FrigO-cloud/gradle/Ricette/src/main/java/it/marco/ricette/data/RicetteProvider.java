package it.marco.ricette.data;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import it.marco.ricette.model.Ingrediente;
import it.marco.ricette.model.Ricetta;
import retrofit.Callback;
import retrofit.RestAdapter;


public class RicetteProvider extends AppCompatActivity {
    private static String desc;
    private static String cate;
    private static String ricing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//    String s = ((RicetteApplication) this.getApplication()).getSomeVariable();
//    String alim = getIntent().getExtras().getString("alim");

    }
//    public final String s = ((RicetteApplication) this.getApplication()).getSomeVariable();
/***
public String load(Context context){
    Log.i("TESTE", "FILE");
    try {
        FileInputStream fis = context.openFileInput("file_name"+".txt");
        BufferedReader r = new BufferedReader(new InputStreamReader(fis));
        String line= r.readLine();
        r.close();
        return line;
    } catch (IOException e) {
        e.printStackTrace();
        Log.i("TESTE", "FILE - false");
        return null;
    }
}
*/
    public static RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://www.t4srl.it/home/api7")
            .build();
    private static RicetteService service = restAdapter.create(RicetteService.class);

//    public static void getAllRicette(Callback<List<Ricetta>> callback) {
    public static void getAllRicette(Callback<List<Ricetta>> callback, Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences("FRIGO", 0); // Maurizio
        desc= settings.getString("descValue", ""); // Maurizio
        cate= settings.getString("cateValue", ""); // Maurizio
        ricing= settings.getString("ricingValue", ""); // Maurizio

//        service.getAllRicette(callback);
        service.getAllRicette2(desc,cate,ricing, callback);
    }

//    public static void getAllIngredienti(Callback<List<Ingrediente>> callback) {
    public static void getAllIngredienti(Callback<List<Ingrediente>> callback, Activity activity) {
//        desc="8047";
//        cate="77";
//        ricing="2";
        SharedPreferences settings = activity.getSharedPreferences("FRIGO", 0); // Maurizio
        desc= settings.getString("descValue", ""); // Maurizio
        cate= settings.getString("cateValue", ""); // Maurizio
        ricing= settings.getString("ricingValue", ""); // Maurizio

//        service.getAllIngredienti( callback);
        service.getAllIngredienti2(desc,cate,ricing, callback);
    }

//    public static void getOneAlimento(Callback<List<Alimento>> callback) {
//        service.getOneAlimento(callback);
//    }

    public static void updateServer(String server) {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(server)
                .build();
        service = restAdapter.create(RicetteService.class);
    }

}
