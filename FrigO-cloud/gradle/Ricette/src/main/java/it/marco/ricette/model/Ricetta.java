package it.marco.ricette.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Ricetta implements Parcelable {
    public String name;
    public String imageUrl;
    public String history;
    public String persone;
    public String instructions;
    public List<String> ingredients = new ArrayList<String>();
    public String wiksponsor;
    public List<String> ingid = new ArrayList<String>();
    public String imageUrlsr;
    public String votiChef;
    public String stelleChef;
    public String votiRicetta;
    public String stelleRicetta;

    public Ricetta() {
    }

    private Ricetta(Parcel parcel) {
        name = parcel.readString();
        imageUrl = parcel.readString();
        history = parcel.readString();
        persone = parcel.readString();
        instructions = parcel.readString();
        parcel.readStringList(ingredients);
        wiksponsor = parcel.readString();
        parcel.readStringList(ingid);
        imageUrlsr = parcel.readString();
        votiChef = parcel.readString();
        stelleChef = parcel.readString();
        votiRicetta = parcel.readString();
        stelleRicetta = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(imageUrl);
        parcel.writeString(history);
        parcel.writeString(persone);
        parcel.writeString(instructions);
        parcel.writeStringList(ingredients);
        parcel.writeString(wiksponsor);
        parcel.writeStringList(ingid);
        parcel.writeString(imageUrlsr);
        parcel.writeString(votiChef);
        parcel.writeString(stelleChef);
        parcel.writeString(votiRicetta);
        parcel.writeString(stelleRicetta);
    }

    public static final Parcelable.Creator<Ricetta> CREATOR = new Parcelable.Creator<Ricetta>() {
        public Ricetta createFromParcel(Parcel in) {
            return new Ricetta(in);
        }

        public Ricetta[] newArray(int size) {
            return new Ricetta[size];
        }
    };
}
